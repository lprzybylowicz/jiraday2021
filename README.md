# Jira Day 2021 materials

Here you can find materials from Jira Day 2021 presentation. If you have any questions don`t hesitate to ask them via <contact@jiraforthepeople.com>.

## Official SIL documentation
Offical docs are located in https://anovaapps.atlassian.net/wiki/spaces/SIL/overview

## Examples
There are a lot of examples that comes with SIL installation. Be sure to check them out, so you gain the awerness of what you are capable of doing in SIL

## File structure
SIL scripts are by default located in JIRA_HOME/silprograms directory.
I recommend to keep your scripts organized by placing them in separate directories divided by the way of implementation: listeners, postfunctions, live_fields etc. You can also group scripts by project.  

```
├── global_functions
│   └── update_issue_picker.sil
├── listeners
│   └── company_change.sil
├── live_fields
│   ├── hooks
│   │   └── hook.sil
│   └── scripts
│       └── issue_template.sil
└── postfunctions
    ├── autowatch.sil
    ├── copy_data_from_contractor.sil
    └── create_invoice.sil
```

## Implementation

All of the SIL features you are in Manage Apps -> Power Scripts section. 

### SIL Listeners
To implement a listener

- add a new listener
- choose an event that will fire your script
- choose your script
- I recommend using technical user for firing the scripts 

### SIL Postfunctions
To implement a postfunctions you should edit your workflow and add SIL posftunction on a desired transition.
You can create recursive transition that points to itself, so it will only be used for firing your script. 

### Live Fields
Live fields let you interact with JIRA UI, for example it can make changes to create/edit screen. 
You can set predefined description, set value of customfields etc. depending on a project or issuetype. To implement Live Fields:

- go to Live Fields panel 
- add new Live Fields configuration
- you can apply LF to specific project(s) or to project category. I recommend the latter, because it`s more flexible
- then you point to the hook script which gonna collect the information about the project you interact with
- from hook.sil you can point right to the script you want to execute or to script dispatcher from where you can execute scripts based on various parameters (projects, screens etc.) 
